# Report: Decoding Protocols of Remote-controlled Electrical Outlets (Resaerch Seminar 2020/2021)

This project was part of the research seminar of Professor Andreas Polze and the Professorship for Operating Systems and Middleware in the winter semester of 2020.

Author: Maximilian Diez

Advisor: Lukas Pirl

## Introduction

The goal of this project was to reverse-engineer the protcol of a set remote-controlled switchable sockets owned by the research group. The remotes and sockets communicate over 433 Mhz and each socket can be paired to multiple remotes and channels.

## Hardware Setup

The proposed tooling hardware setup consists of the following:

| Name                | Image                                                      |
| ------------------- | ---------------------------------------------------------- |
| Raspberry Pi (e.g. Model B v1.1) | ![Raspberry Pi (e.g. Model B v1.1)](raspberry_pi.jpg) |
| 433 Mhz transmitter | ![433 Mhz transmitter back and front](433_transmitter.jpg) |
| 433 Mhz receiver    | ![433 Mhz transmitter back and front](433_receiver.jpg)    |
| Generic breadboard and cables | ![Cables and breadboard](cables_breadboard.jpg) |

The components should be connected in the following way:

![Setup of Tool Hardware](hardware_setup.jpg)

However, any similar hardware setup that is able to transmit and receive 433 Mhz signals should also work. However, the software tools mentioned throughout this report were only thested with the above hardware setup.

## Protocol Analysis

### Example Hardware

The examplary hardware worked with in the course of this report is a switchable socket set by hardware brand [Mumbi](https://www.mumbi.de). It consists of RCS-20GS sockets and RC-10 remote control.  

| Name                | Tag                                                      | Images  |
| ------------------- | ---------------------------------------------------------- | ---------------- |
| Mumbi RCS-20GS | ![Mumbi RCS-20GS tag](mumbi_socket.jpg) |  ![Mumbi RCS-20GS front and side](mumbi_socket_front_side.jpg) |
| 433 Mhz receiver    | ![Mumbi RC-10 tag](mumbi_remote.jpg)    |  ![Mumbi RC-10 front](mumbi_remote_front.jpg)  |

However, any similar hardware hardware communicating on 433 Mhz with similar protocols should work.

### Record Commands

The first step in reverse engineering the protocol is recording command codes sent by the remote. Since the protocol is non-interactive (i.e., the sockets only listen to commands and do not transmit themselves), it is enough to record the remote independently of the socket.

In order to do so, the tool [SimpleRcScanner](https://github.com/sui77/SimpleRcScanner) can be used. It allows for recording and visualising simple wireless protocols based on [On-Off-Keying](https://en.wikipedia.org/wiki/On-off_keying)/[Amplitute-Shift Keying (ASK)](https://en.wikipedia.org/wiki/Amplitude-shift_keying). A 433 Mhz receiver needs to be correctly attached to the Raspberry Pi via GPIO.

![Recorded command](command.jpg)

### Decode Commands

Depending on the protocol's representation of data (ones and zeroes), the list of times between egdes (high to low and low to high, respectively) obtained from SimpleRcScanner can be used to determine the protocol. For the Mumbi example hardware, the protocol used is a flavor of a [Return-to-Zero](https://en.wikipedia.org/wiki/Return-to-zero) code. Ones and zeroes are encoded with different ratios between high to low and low to high levels, respectively. The ratios differ from implementation to implementation.

![Illustration of 0/1 encoding](0_1_encoding.jpg)

The illustration above highlights the encoding of a zero in red and the encoding of a one in oragange. In the example, zero is encoded by an early (at 291 microseconds) edge between high and low, whereas one is encoded by a late (at 910 microseconds) flank.

![Recorded command with preceding and succeeding command](full_command.jpg)

When recording more than one command, one notices a long continuous low signal at the beginning of each command. This is the syncronisation phase and also differs from implementation to implementation.

The protocol can be fully represented by a tuple consisting of

- pulse length in microseconds
- number of high pulses for syncronisation
- number of low pulses for syncronisation
- number of high pulses for zero
- number of low pulses for zero
- number of high pulses for one
- number of low pulses for one

By inspection of more the recorded command shown above, the protocol of the example hardware can be characterised as 100, 3, 100 3, 8 8, 3. If deducing these values is not possible, one can calculate the values by first recording mutiple commands, splitting them into zeroes, ones and syncronisations, subsequently calculating the average for high and low durations and finally calculating the lowest common denominator of all to acquire the pulse lenght. 

In order to work with different recorded commands efficiently, one can use a library in which protocols can be defined and which allows to record commands from wireless transmissions and assign it a likely value. As is vissible in the illustrations of the recorded wireless trnsmissions above, not all ones have the exact same edge position. Therefore, some error correction is necessary. The library being used in the following is [rc-switch](https://github.com/sui77/rc-switch). The protocol of the example hardware can be represented in rc-switch by defining a protocol struct `{ 100, { 3, 100 }, { 3, 8 }, { 8, 3 }, false }`. Note that the last binary value in the struct decodes whether the protocol is inverted (i.e., low to high rather than high to low), which is evidently not the case for the example hardware.

Note that the protocol of the example hardware also requires code words longer than 32 bit. A [fork]([here](https://github.com/Sofoca/rc-switch)) adding support for 64 bit codes as well as the protocol of the example hardware has been created in the course of this project.

### Deconstruct Commands

In order to derive the meaning of codes sent by the remote control, one first needs to record their values. [433Tools](https://github.com/ninjablocks/433Utils)'s RFSNiffer provides a command line interface based on rc-switch and prints the recoded wireless transmission to the command line as decimal numbers. After recording these and converting them to binary, the following set of commands can be obtained. They include recordings of two devices by OSM as well as two sets of commands obtained on the internet and verified by transmitting them to the switches.

| Remote | Channel | Command | Binary code |
| ------ | ------- | ------- | ----------- |
| OSM L  | A       | On      | `0000 1000 1000 0010 0000 111 1 1000 001 0 0` |
| OSM L  | A       | Off     | `0000 1000 1000 0010 0000 111 0 1000 001 1 0` |
| OSM L  | B       | On      | `0000 1000 1000 0010 0000 110 1 1000 000 0 0` |
| OSM L  | B       | Off     | `0000 1000 1000 0010 0000 110 0 1000 000 1 0` |
| OSM L  | C       | On      | `0000 1000 1000 0010 0000 101 1 1000 010 0 0` |
| OSM L  | C       | Off     | `0000 1000 1000 0010 0000 101 0 1000 010 1 0` |
| OSM L  | D       | On      | `0000 1000 1000 0010 0000 011 1 1000 110 0 0` |
| OSM L  | D       | Off     | `0000 1000 1000 0010 0000 011 0 1000 110 1 0` |
| OSM L  | ALL       | On    | `0000 1000 1000 0010 0000 010 0 1000 111 0 0` |
| OSM L  | ALL       | Off   | `0000 1000 1000 0010 0000 100 0 1000 011 0 0` |

| Remote | Channel | Command | Binary code |
| ------ | ------- | ------- | ----------- |
| OSM R  | A       | On      | `0000 1110 1100 1000 0000 111 1 1100 111 0 0` |
| OSM R  | A       | Off     | `0000 1110 1100 1000 0000 111 0 1100 111 1 0` |
| OSM R  | B       | On      | `0000 1110 1100 1000 0000 110 1 1100 110 0 0` |
| OSM R  | B       | Off     | `0000 1110 1100 1000 0000 110 0 1100 110 1 0` |
| OSM R  | C       | On      | `0000 1110 1100 1000 0000 101 1 1100 101 0 0` |
| OSM R  | C       | Off     | `0000 1110 1100 1000 0000 101 0 1100 101 1 0` |
| OSM R  | D       | On      | `0000 1110 1100 1000 0000 011 1 1100 011 0 0` |
| OSM R  | D       | Off     | `0000 1110 1100 1000 0000 011 0 1100 011 1 0` |
| OSM R  | ALL       | On    | `0000 1110 1100 1000 0000 010 0 1100 010 1 0` |
| OSM R  | ALL       | Off   | `0000 1110 1100 1000 0000 100 0 1100 100 1 0` |

| Remote | Channel | Command | Binary code |
| ----------------------------------------------------------- | ------ | ------- | ----------- |
| [Internet 1](https://www.mikrocontroller.net/topic/424182) | A       | On      | `0110 0001 0111 0001 0000 111 1 0010 000 0 0` |
| [Internet 1](https://www.mikrocontroller.net/topic/424182) | A       | Off     | `0110 0001 0111 0001 0000 111 0 0010 000 1 0` |
| [Internet 1](https://www.mikrocontroller.net/topic/424182) | B       | On      | `0110 0001 0111 0001 0000 110 1 0010 001 1 0` |
| [Internet 1](https://www.mikrocontroller.net/topic/424182) | B       | Off     | `0110 0001 0111 0001 0000 110 0 0010 001 0 0` |
| [Internet 1](https://www.mikrocontroller.net/topic/424182) | C       | On      | `0110 0001 0111 0001 0000 101 1 0010 011 1 0` |
| [Internet 1](https://www.mikrocontroller.net/topic/424182) | C       | Off     | `0110 0001 0111 0001 0000 101 0 0010 011 0 0` |
| [Internet 1](https://www.mikrocontroller.net/topic/424182) | D       | On      | `0110 0001 0111 0001 0000 011 1 0010 111 1 0` |
| [Internet 1](https://www.mikrocontroller.net/topic/424182) | D       | Off     | `0110 0001 0111 0001 0000 011 0 0010 111 0 0` |
| [Internet 1](https://www.mikrocontroller.net/topic/424182) | ALL     | On      | `0110 0001 0111 0001 0000 010 0 0010 110 0 0` |
| [Internet 1](https://www.mikrocontroller.net/topic/424182) | ALL     | Off     | `0110 0001 0111 0001 0000 100 0 0010 010 0 0` |

| Remote | Channel | Command | Binary code |
| ----------------------------------------------------------- | ------ | ------- | ----------- |
| [Internet 2](https://github.com/sui77/rc-switch/wiki/Description-of-socket-protocols-from-different-brands-and-models#arlec-rc210-aunz-240v-socket-remote-controlled-power-outlets) | A       | On      | `0111 0110 1101 1000 0000 111 1 1001 111 0 0` |
| [Internet 2](https://github.com/sui77/rc-switch/wiki/Description-of-socket-protocols-from-different-brands-and-models#arlec-rc210-aunz-240v-socket-remote-controlled-power-outlets) | A       | Off     | `0111 0110 1101 1000 0000 111 0 1001 111 1 0` |
| [Internet 2](https://github.com/sui77/rc-switch/wiki/Description-of-socket-protocols-from-different-brands-and-models#arlec-rc210-aunz-240v-socket-remote-controlled-power-outlets) | B       | On      | `0111 0110 1101 1000 0000 110 1 1001 110 0 0` |
| [Internet 2](https://github.com/sui77/rc-switch/wiki/Description-of-socket-protocols-from-different-brands-and-models#arlec-rc210-aunz-240v-socket-remote-controlled-power-outlets) | B       | Off     | `0111 0110 1101 1000 0000 110 0 1001 110 1 0` |
| [Internet 2](https://github.com/sui77/rc-switch/wiki/Description-of-socket-protocols-from-different-brands-and-models#arlec-rc210-aunz-240v-socket-remote-controlled-power-outlets) | C       | On      | `0111 0110 1101 1000 0000 101 1 1001 101 0 0` |
| [Internet 2](https://github.com/sui77/rc-switch/wiki/Description-of-socket-protocols-from-different-brands-and-models#arlec-rc210-aunz-240v-socket-remote-controlled-power-outlets) | C       | Off     | `0111 0110 1101 1000 0000 101 0 1001 101 1 0` |
| [Internet 2](https://github.com/sui77/rc-switch/wiki/Description-of-socket-protocols-from-different-brands-and-models#arlec-rc210-aunz-240v-socket-remote-controlled-power-outlets) | D       | On      | `0111 0110 1101 1000 0000 011 1 1001 011 0 0` |
| [Internet 2](https://github.com/sui77/rc-switch/wiki/Description-of-socket-protocols-from-different-brands-and-models#arlec-rc210-aunz-240v-socket-remote-controlled-power-outlets) | D       | Off     | `0111 0110 1101 1000 0000 011 0 1001 011 1 0` |
| [Internet 2](https://github.com/sui77/rc-switch/wiki/Description-of-socket-protocols-from-different-brands-and-models#arlec-rc210-aunz-240v-socket-remote-controlled-power-outlets) | ALL     | On      | `0111 0110 1101 1000 0000 100 0 1001 100 1 0` |

By inspecting the different codes and their semantic (remote, channel and command), the following parts of the command codes can be identified. For better readbility, bits are numbered from left to right.

- Bit 0-19: ID
- Bit 20-22: Channel
- Bit 23: 1 iff `on` (channel A-D), always 0 (channel ALL)
- Bit 24-27: `f1(ID)`
- Bit 28-30: `f2(ID,Channel)`
- Bit 31: 0 iff `on` (channel A-D), unclear/irrelevant (channel ALL)
- Bit 32: always 0

Due to their length, one can assume that bit 24-27 are an authentication of the ID (such as a message integrity code) or a checksum dependent on the ID. The same holds for bit 28-30 about the channel and the ID. Note that bit 23 and 31 toggle switching on or off for channels. When adressing the ALL channel, bit 23 is always 0, whereas bit 31 depends on the ID. However, by testing one can ascertain that bit 31 is actually irrelevant in this case and either will be accepted by the example sockets.

### Replay Codes

Based on the recorded commands, the function of a specific remote can already be fully mocked. The rc-switch `send(long code, int length)` function offers the most suitable interface for this. A 433 Mhz transmitter needs to be correctly attached to the Raspberry Pi via GPIO. In order to increase the transmission strength, a curled wire can be used as a makeshift antenna (see hardware setup image).

### Alter Codes

The protocol offers two possibilities for altering existing commands: ID and channel. Unfortunately, changing either generally results in commands not being accepted by the sockets.

A strong possibility is that this is due to bits 24-30 being dependent on ID (bit 0-19) and channel (bit 28-30). One can try to modulate these parts depending on changes to ID and channel, respectively. However, functions used for simple checksums such as increments, XORs or ANDs do not result in correct commands. If this part of the commands is used for authentication, it likely contains a secret key which cannot easily be analysed (certainly not with the limited set of examples available). Unfortunately, the chip that likely encodes/decodes commands on neither the remotes' nor the switches' boards is marked.

Note that the bits that toggle switching on and off are not part of either checksum/authentication. Therefore, the off command can be derived from an on command and vice-versa.

One noteworthy exception are the ID and `f1(ID)` of both OSM remotes. Bits 8-11 and bits 24-27 are exactly equal and experimentation shows that they can be changed as long as they are changed equally. The command is still valid and accepted by the remote. This suggests that in those two cases, the value of the rest of the ID results in this part exclusively defining the result of `f1(ID)` (i.e., the rest of the ID results in `f1(ID)=id(ID[8:11])`). Furthermore, `f1(Channel,ID)` is not impacted by these cahnges either, suggesting that bits 8-11 do not impact `f2(ID,Channel)`. These two exceptions overall result in another 30 possiblites for IDs.

Altering the channel (the 3 bits would thoretically allow for another 2 channels) does not work, even in cases where `f2(ID,Channel)=Channel` for all cases. Likely, the sockets are pre-programmed for 4 channels (and another 2 values representing ALL).

Last but not least, it is indistinguishible which parts of bits 0-22 and 24-30 are deciding and which are parts are merely results of functions or otherwise used. Crucially, changing anything in these areas results in codes being rejected (with the exemptions mentioned above).

## Application Example: Marvis Bridge

Now that 34 "virtual remotes" can be fully mocked by just one Raspberry Pi, the sockets can be used in novel applications. One example is using them as part of the open-source hybrid testbed [marvis](https://github.com/diselab/marvis). Enabling the integration of physical power on/power off into automated testing workflows promises new possibilities for testing scenarios and a better control over physcial nodes in hybrid test setups. For exmple, error injection by not only software-based halting by instantly powering down devices could provide more realistic results.

A proof-of-concept implementation of this application example has been implemented and published in [this pull request](https://github.com/diselab/marvis/pull/1). It includes a wrapper class to add the functionality to existing testing workflow objects as well as a custom server that runs on a devices in physcial proximity to the sockets that offers a simple HTTP interface and converts requests to commands immediately transmitted over 433 Mhz to the sockets.
