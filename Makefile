MDs ?= report.md
PDFs ?= $(MDs:.md=.pdf)

MDTOPDF ?= node_modules/.bin/md-to-pdf \
	--launch-options '{ "args": ["--no-sandbox"] }'

MAKEFLAGS += -r

all: $(PDFs)

%.tmp.ps: %.tmp.pdf
	pdf2ps $^

%.tmp.pdf: %.md
	cat $^ | $(MDTOPDF) > $@

%.pdf: %.tmp.ps
	ps2pdf $^ $@

clean:
	rm -f *.tmp.ps *.tmp.pdf

distclean: clean
	rm -f $(PDFs)
