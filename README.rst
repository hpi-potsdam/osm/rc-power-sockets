.. image:: https://gitlab.com/hpi-potsdam/osm/rc-power-sockets/badges/main/pipeline.svg
  :target: https://gitlab.com/hpi-potsdam/osm/rc-power-sockets/pipelines
  :align: right

A report on reverse engineering the protocol of remote-controlled
electrical power outlets (`Research Seminar 2020/2021
<https://osm.hpi.de/research-seminar/2020-2021/>`__).

`report <report.md>`__

`report as PDF <https://gitlab.com/hpi-potsdam/osm/rc-power-sockets/-/jobs/artifacts/main/raw/report.pdf?job=pdf>`__

`slides
<https://osm.hpi.de/research-seminar/2020-2021/protected/2021_02_23__MaximilianDiez_Final_Presentation_Decoding_Protocols_of_Remote_Controlled_Electrical_Outlets.pdf>`__
